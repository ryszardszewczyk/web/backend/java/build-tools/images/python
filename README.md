# python image

A python image used to perform operations associated with GitLab through its api.

| Available commands | Short description                            | Description with history of changes                      | Link to file                                  |
| ------------------ | -------------------------------------------- | -------------------------------------------------------- | --------------------------------------------- |
| images-badges      | Posts a badge about the version of the image | [Description of images-badges](#images-badges)           | [images-badges](./images-badges.py)           |
| images-topics      | Adds a topic based on a dockerfile directory | [Description of images-topics](#images-topics)           | [images-topics](./images-topics.py)           |
| labels             | Adds a label based on a changed file         | [Description of labels](#labels)                         | [labels](./labels.py)                         |
| codeclimate2gitlab | Adds detected defections to Gitlab reviews   | [Description of codeclimate2gitlab](#codeclimate2gitlab) | [codeclimate2gitlab](./codeclimate2gitlab.py) |

## images-badges:

Provides a way to add or update a badge to tell what docker image version is the current one.
It requires given arguments:

| No  | Argument     | Description                                  |
| --- | ------------ | -------------------------------------------- |
| 1.  | --url        | API address of GitLab                        |
| 2.  | --token      | Authentication token                         |
| 3.  | --project-id | Unique GitLab project ID                     |
| 4.  | --image      | Name of the image published to Docker Hub    |
| 5.  | --version    | Version of the image published to Docker Hub |
| 6.  | --base       | Main branch name                             |

You can either use it as subprocess command or a job as below:

```yaml
.badges:
  image: bananawhite98/python:1
  stage: deploy
  tags:
    - gitlab-org-docker
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      changes:
        - ${DOCKER_IMAGE}/* # This is a way to tell GitLab to only fire jobs extending this one, when changes are made to directory where Dockerfile is located.
  script:
    - >
      images-badges.py
      --url "${CI_SERVER_URL}"
      --token "${CI_GITLABBOT}"
      --project-id "${CI_PROJECT_ID}"
      --image "${DOCKER_IMAGE}"
      --version "${VERSION}"
      --base "${CI_DEFAULT_BRANCH}"
```

This job is only a proposal of the way to use, you can modify the way you want, nonetheless script has to conform to the one in the example.

## images-topics

Provides a way to add topics based on a name of a directory where Dockerfile resides.
It requires those arguments:

| No  | Argument     | Description              |
| --- | ------------ | ------------------------ |
| 1.  | --url        | API address of GitLab    |
| 2.  | --token      | Authentication token     |
| 3.  | --project_id | Unique GitLab project ID |
| 4.  | --image      | Name of the image        |

You can either use it as a subprocess command or a job as shown below:

```yaml
.topics:
  image: bananawhite98/python:1
  stage: deploy
  tags:
    - gitlab-org-docker
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      changes:
        - ${DOCKER_IMAGE}/*
  script:
    - >
      images-topics.py
      --url "${CI_SERVER_URL}"
      --token "${CI_GITLABBOT}"
      --project_id "${CI_PROJECT_ID}"
      --image "${DOCKER_IMAGE}
```

## labels

Provides a way to add a label based on a changed file. It is for adding labels for openapi yml files.
For example, we change a file: user.openapi.yml, bot should add a label user:api for that merge request.
Now, it supports adding labels for given changed files:

1. .openapi.yml
2. .sql
3. .java
4. .python

It requires given arguments:

| No  | Argument          | Description           |
| --- | ----------------- | --------------------- |
| 1.  | --url             | API Address of GitLab |
| 2.  | --base-sha        | Base sha              |
| 3.  | --token           | Authentication token  |
| 4.  | --project-id      | Project id            |
| 5.  | --mergerequest-id | Merge request id      |

You can either use it as a subprocess command or a job as shown below:

```yaml
labels:
  image: bananawhite98/python:1
  stage: test
  tags:
    - gitlab-org-docker
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
  script:
    - >
      labels.py
      --url "${CI_SERVER_URL}"
      --base-sha "${CI_MERGE_REQUEST_DIFF_BASE_SHA}"
      --token "${CI_GITLABBOT}"
      --project-id "${CI_PROJECT_ID}"
      --mergerequest-id "${CI_MERGE_REQUEST_IID}"
```

## codeclimate2gitlab

Provides a way to add a comment containing a defect in a form of Code Climate.
It requires those arguments:

| No  | Argument | Description                   |
| --- | -------- | ----------------------------- |
| 1.  | --url    | Gitlab API address            |
| 2.  | --token  | Authentication token          |
| 3.  | --pr-id  | Project ID                    |
| 4.  | --mr-id  | Merge Request ID              |
| 5.  | --input  | File with Code Climate report |
