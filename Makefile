SHELL := bash
DOCKERFILE := Dockerfile
DOCKER_CONTEXT := .
DOCKER_NAME = bananawhite98/python-linux
GITLAB_DOCKER_NAME = registry.gitlab.com/ryszardszewczyk/build-tools/images/python/python-linux

VERSION := $(shell tag=$$(git describe --tags --abbrev=0 2>/dev/null); if [[ $$? -ne 0 ]]; then echo 0.0.$$(git rev-list --all --count); else echo $${tag}.$$(git rev-list $${tag}.. --count); fi;)
DOCKER_NAME_FULL = $(DOCKER_NAME):$(VERSION)
DOCKER_NAME_LATEST = $(DOCKER_NAME):1
PROJECT_ID = 58212930
REPOSITORY_ID = 6739978

.PHONY: docker-build-all

gitlab-build:
	docker build --pull -f $(DOCKERFILE) $(DOCKER_CONTEXT) -t $(GITLAB_DOCKER_NAME):$(TASK_NUMBER)

gitlab-push:
	docker push $(GITLAB_DOCKER_NAME):$(TASK_NUMBER)

gitlab-rmi:
	docker rmi -f $(GITLAB_DOCKER_NAME):$(TASK_NUMBER)

docker-build: $(DOCKERFILE)
	docker pull $(GITLAB_DOCKER_NAME):$(TASK_NUMBER)
	docker tag $(GITLAB_DOCKER_NAME):$(TASK_NUMBER) $(DOCKER_NAME_FULL)
	docker tag $(DOCKER_NAME_FULL) $(DOCKER_NAME_LATEST)

docker-push:
	docker push $(DOCKER_NAME_FULL)
	docker push $(DOCKER_NAME_LATEST)

docker-rmi:
	docker rmi -f $(GITLAB_DOCKER_NAME):$(TASK_NUMBER)
	docker rmi -f $(DOCKER_NAME_FULL)
	docker rmi -f $(DOCKER_NAME_LATEST)

docker-gitlab-rmi:
	docker run $(DOCKER_NAME_LATEST) bash -c "gitlab-registry-docker.py --url '$(CI_SERVER_URL)' --token '$(CI_GITLABBOT)' --project-id $(PROJECT_ID) --repository-id $(REPOSITORY_ID) --tag '$(TASK_NUMBER)'"

print:
	@echo "Python image version is: $(VERSION)"