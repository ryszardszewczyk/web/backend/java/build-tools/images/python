$DOCKERFILE = ".\Dockerfile.windows"
$DOCKER_NAME = "bananawhite98/python-win"
$GITLAB_DOCKER_NAME = "registry.gitlab.com/ryszardszewczyk/build-tools/images/python/python-windows"

$VERSION = git describe --tags --abbrev=0 2>$null
if ($LASTEXITCODE -ne 0) {
    $VERSION = "0.0.$(git rev-list --all --count)"
} else {
    $VERSION = "$VERSION.$(git rev-list $VERSION.. --count)"
}
$DOCKER_NAME_FULL = "${DOCKER_NAME}:${VERSION}"
$DOCKER_NAME_LATEST = "${DOCKER_NAME}:1"
$PROJECT_ID = 58212930
$REPOSITORY_ID = 6739995

Function GitLab-Build
{
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$true)]
        [string]$TASK_NUMBER
    )
    process {
        docker build --pull -f $DOCKERFILE . -t "${GITLAB_DOCKER_NAME}:${TASK_NUMBER}"
    }
}

Function GitLab-Push
{
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$True)]
        [string]$TASK_NUMBER
    )
    process {
        docker push "${GITLAB_DOCKER_NAME}:${TASK_NUMBER}"
    }
}

Function GitLab-Rmi
{
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$True)]
        [string]$TASK_NUMBER
    )
    process {
        docker rmi -f "${GITLAB_DOCKER_NAME}:${TASK_NUMBER}"
    }
}

Function Docker-Build
{
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$True)]
        [string]$TASK_NUMBER
    )
    process {
        docker pull "${GITLAB_DOCKER_NAME}:${TASK_NUMBER}"
        docker tag "${GITLAB_DOCKER_NAME}:${TASK_NUMBER}" $DOCKER_NAME_FULL
        docker tag $DOCKER_NAME_FULL $DOCKER_NAME_LATEST
    }
}

Function Docker-Push
{
    process {
        docker push $DOCKER_NAME_FULL
        docker push $DOCKER_NAME_LATEST
    }
}

Function Docker-Rmi
{
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$True)]
        [string]$TASK_NUMBER
    )
    process {
        docker rmi -f "${GITLAB_DOCKER_NAME}:${TASK_NUMBER}"
        docker rmi -f $DOCKER_NAME_FULL
        docker rmi -f $DOCKER_NAME_LATEST
    }
}

Function Print
{
    process {
        Write-Output "python image version is: ${VERSION}"
    }
}

Function Docker-GitLab-Rmi
{
    [CmdletBinding(SupportsShouldProcess=$True)]
    param (
        [Parameter(Mandatory=$True)]
        [string]$TASK_NUMBER,
        [string]$CI_SERVER_URL,
        [string]$CI_GITLABBOT
    )
    process {
        docker run $DOCKER_NAME_LATEST powershell "gitlab-registry-docker.py --url ${CI_SERVER_URL} --token ${CI_GITLABBOT} --project-id ${PROJECT_ID} --repository-id ${REPOSITORY_ID} --tag ${TASK_NUMBER}"
    }
}