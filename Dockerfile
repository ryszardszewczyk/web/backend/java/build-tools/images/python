FROM python:3

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

COPY files/*.py /usr/bin

RUN chmod +x /usr/bin/*.py \
    && git clone --depth=1 https://github.com/sourcegraph/lsif-py /opt/lsif-py

WORKDIR /opt/lsif-py

RUN pip install --no-cache-dir jedi==0.17.2 \
    && pip install --no-cache-dir anybadge \
    && pip install --no-cache-dir python-gitlab \
    && pip install --no-cache-dir black \
    && pip install --no-cache-dir --upgrade pyyaml==6.0.1 \
    && pip install --no-cache-dir --upgrade poetry \
    && pip install --no-cache-dir GitPython \
    && apt-get clean autoclean --yes \
    && apt-get autoremove --yes \
    && rm -rf /var/lib/{cache,log}/ \
    && rm -rf /tmp/* \
