#!/usr/bin/env python3
from argparse import ArgumentParser, Namespace

from gitlab.v4.objects import Project

from gitlab_function import authenticate
from gitlab_function import project
from base import logger

LOGGER = logger()


def args() -> Namespace:
    """
    Creates an ArgumentParser to run creating topics.
    :return: A Namespace instance.
    """
    parser = ArgumentParser(
        description="Bot automating adding topics based on Dockerfiles' folders"
    )
    parser.add_argument("--url", help="API address of GitLab", required=True, type=str)
    parser.add_argument("--token", help="Authentication token", required=True, type=str)
    parser.add_argument(
        "--project_id", help="Unique GitLab project ID", required=True, type=int
    )
    parser.add_argument("--image", help="Name of the image", required=True, type=str)
    return parser.parse_args()


def check_if_topic_exists(pr: Project, topic: str) -> bool:
    """
    Check if given topic is added the project information.
    :param pr: Project instance
    :param topic: Topic name to be checked
    :return: If topic is added return True otherwise False.
    """
    LOGGER.info("Checking if given topic '%s' exists", topic)
    topics = pr.topics
    LOGGER.info("Topics %s", topics)
    for tpc in topics:
        if tpc == topic:
            LOGGER.info("Found a topic '%s', skipping", topic)
            return True
    LOGGER.info("Couldn't find a topic '%s'", topic)
    return False


def create_topic(pr: Project, topic: str) -> None:
    """
    Adds a topic to the project.
    :param pr: Project instance
    :param topic: Topic name to be added
    :return: None.
    """
    LOGGER.info("Appending topic '%s'", topic)
    pr.topics.append(topic)
    pr.save()


def main():
    cmd = args()
    gl = authenticate(cmd.url, cmd.token)
    pr = project(gl, cmd.project_id)
    if check_if_topic_exists(pr, cmd.image) is False:
        LOGGER.info("Topic: %s not found, adding it", cmd.image)
        create_topic(pr, cmd.image)


if __name__ == "__main__":
    main()
