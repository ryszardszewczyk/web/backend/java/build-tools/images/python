#!/usr/bin/env python3
import logging
from logging import Logger


def logger() -> Logger:
    """
    Creates a logger
    :return: Logger instance itself
    """
    _logger = logging.getLogger(__file__)
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    )
    return _logger
