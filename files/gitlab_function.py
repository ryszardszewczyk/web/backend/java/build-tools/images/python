#!/usr/bin/env python3
from base import logger

from gitlab import Gitlab
from gitlab.v4.objects import Project
from gitlab.v4.objects import ProjectMergeRequest
from gitlab.v4.objects import RegistryRepository

LOGGER = logger()


def authenticate(url: str, token: str) -> Gitlab:
    """
    Enables authenticating with GitLab.
    :param url: GitLab url
    :param token: Access token with read api
    :return: Gitlab instance itself
    """
    LOGGER.info("Authenticating with GitLab")
    gl = Gitlab(url, private_token=token)
    gl.auth()
    if gl._objects.CurrentUser is None:
        LOGGER.warning("Cannot authenticated with GitLab")
    else:
        LOGGER.info("Successfully authenticated with GitLab")
    return gl


def project(gl: Gitlab, pr_id: int) -> Project:
    """
    Returns a project information based on provided project id.
    :param gl: GitLab instance
    :param pr_id: Project id
    :return: Project instance.
    """
    return gl.projects.get(id=pr_id)


def container_registry_repository(pr: Project, rp_id: int) -> RegistryRepository | None:
    repos = pr.repositories.list()
    for repo in repos:
        if repo.id == rp_id:
            return repo
    return None


def merge_request(pr: Project, mr_id: int) -> ProjectMergeRequest:
    """
    Returns associated merge request with given project.
    :param pr: Project instance
    :param mr_id: Merge Request ID
    :return: ProjectMergeRequest instance.
    """
    return pr.mergerequests.get(mr_id)
