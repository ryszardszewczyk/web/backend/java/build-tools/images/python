#!/usr/bin/env python3
import json
from argparse import ArgumentParser, FileType, Namespace
from collections import defaultdict
from sys import exit

from gitlab.v4.objects import ProjectMergeRequest

from gitlab_function import project
from gitlab_function import authenticate
from gitlab_function import merge_request

from base import logger

LOGGER = logger()


def args() -> Namespace:
    """
    Creates an ArgumentParser to run adding comments containing defects in form of code climate.
    :return: A Namespace instance.
    """
    parser = ArgumentParser(
        description="Adds detected defections in Code Climate format to Gitlab reviews in forms of comments"
    )
    parser.add_argument("--url", help="Gitlab API address", required=True, type=str)
    parser.add_argument("--token", help="Authentication token", required=True, type=str)
    parser.add_argument("--pr-id", help="Project ID", required=True, type=int)
    parser.add_argument("--mr-id", help="Merge Request ID", required=True, type=int)
    parser.add_argument(
        "--input",
        help="File with Code Climate report",
        required=True,
        type=FileType("r"),
        default="-",
    )
    return parser.parse_args()


def get_diff_discussions(mr: ProjectMergeRequest) -> defaultdict:
    """
    Gets all discussions presented as a difference note.
    :param mr: Project merge request
    :return: Dictionary with default factory.
    """
    diff_discussions = defaultdict(set)

    for discussion in mr.discussions.list(all=True):
        parent_note = discussion.attribute["notes"][0]
        if parent_note["type"] != "DiffNote":
            continue

        position = parent_note["position"]
        path = position.get("new_path") or position["old_path"]
        line = position.get("new_line") or position["old_line"]
        diff_discussions[f"{path}:{line}"].add(parent_note["body"])
    return diff_discussions


def create_comment_payload(defect: dict, last_diff) -> dict:
    """
    Creates a comment payload.
    :param defect: Located defect
    :param last_diff: Last time comment was created
    :return: Dictionary of the comment payload.
    """
    fingerprint = defect["fingerprint"]
    rule = defect["check_name"]
    desc = defect["description"]
    path = defect["location"]["path"]
    line = defect["location"]["positions"]["begin"]["line"]
    body = f"{fingerprint}: {rule}\n\n{desc}"
    return {
        "body": body,
        "position": {
            "position_type": "text",
            "base_sha": last_diff.base_commit_sha,
            "head_sha": last_diff.head_commit_sha,
            "start_sha": last_diff.start_commit_sha,
            "new_path": path,
            "new_line": line,
        },
    }


def publish_comments(defects: list[dict], mr: ProjectMergeRequest) -> None:
    diff_discussions = get_diff_discussions(mr)
    last_diff = mr.diffs.list(per_page=1)[0]
    for defect in defects:
        payload = create_comment_payload(defect, last_diff)
        path = payload["position"]["new_path"]
        line = payload["position"]["new_line"]

        if payload["body"] in diff_discussions[f"{path}:{line}"]:
            LOGGER.debug(
                "Skipping already reported defects %s at file %s:%s",
                defect["check_name"],
                path,
                line,
            )
            continue

        discussion = mr.discussions.create(payload)
        LOGGER.debug("Created discussion %s", discussion)
    exit(int(len(defects) != 0))


def main():
    cmd = args()
    data = json.load(cmd.input)
    gl = authenticate(cmd.url, cmd.token)
    pr = project(gl, cmd.pr_id)
    mr = merge_request(pr, cmd.mr_id)
    publish_comments(data, mr)
