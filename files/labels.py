#!/usr/bin/env python3
import subprocess
from argparse import ArgumentParser, Namespace
from os.path import splitext
from pathlib import Path

from gitlab_function import authenticate
from gitlab_function import project
from gitlab_function import merge_request

from base import logger

LOGGER = logger()

SUPPORTED_LABELS = {"api", "ci"}


def args() -> Namespace:
    """
    Creates an ArgumentParser to run creating topics.
    :return: A Namespace instance.
    """
    parser = ArgumentParser(
        description="Bot automatyzujący różnorakie czynności na GitLabie"
    )
    parser.add_argument("--url", help="API Address of GitLab", required=True, type=str)
    parser.add_argument("--base-sha", help="Base sha", required=True, type=str)
    parser.add_argument("--token", help="Authentication token", required=True, type=str)
    parser.add_argument(
        "--project-id", help="GitLab project id", required=True, type=int
    )
    parser.add_argument(
        "--mergerequest-id",
        help="Merge request id",
        required=True,
        type=int,
    )
    return parser.parse_args()


def update_labels(
    labels: set[str], gitlab_url: str, token: str, proj_id: int, mr_id: int
) -> None:
    """
    Updates labels for the associated merge request.
    :param labels: Labels to add
    :param gitlab_url: URL of Gitlab
    :param token: Authorization token
    :param proj_id: Project id
    :param mr_id: Merge request id
    :return: None
    """
    gl = authenticate(gitlab_url, token)
    pr = project(gl, proj_id)
    mr = merge_request(pr, mr_id)

    old_labels = set(mr.labels)
    for mr_label in mr.labels:
        if mr_label.startswith("api:") or mr_label in SUPPORTED_LABELS:
            continue
        labels.add(mr_label)
    if old_labels == labels:
        LOGGER.debug("No need to update labels %s", labels)
    mr.labels = sorted(labels)
    mr.save()


def detected_labels(base_sha: str) -> set[str]:
    """
    Detects labels to add based on changed files.
    :param base_sha: Head of the base branch
    :return: Returns set of labels to be added.
    """
    resp = subprocess.run(
        ["git", "diff", "--name-only", base_sha + ".."],
        check=True,
        stdout=subprocess.PIPE,
    )
    files = set(resp.stdout.decode("utf8").rstrip().split("\n"))
    api_files = [file for file in files if file.endswith(".openapi.yml")]
    extensions = {splitext(file)[1] for file in files}
    labels = set()
    for api_file in api_files:
        path = Path(api_file)
        label = "api:" + path.name.partition(".")[0]
        labels.add(label)
    if labels:
        labels.add("api")
    if ".gitlab-ci.yml" in files:
        labels.add("ci")
    if ".sql" in extensions:
        labels.add("sql")
    if ".java" in extensions:
        labels.add("java")
    if ".py" in extensions:
        labels.add("python")
    LOGGER.info("Detected given labels %s", labels)
    return labels


def main():
    cmd = args()
    labels = detected_labels(cmd.base_sha)
    update_labels(labels, cmd.url, cmd.token, cmd.project_id, cmd.mergerequest_id)


if __name__ == "__main__":
    main()
