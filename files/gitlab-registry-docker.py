#!/usr/bin/env python3
from argparse import ArgumentParser, Namespace

from gitlab_function import authenticate, project, container_registry_repository

from base import logger

LOGGER = logger()


def args() -> Namespace:
    parser: ArgumentParser = ArgumentParser(
        description="Bot automating deleting tags from GitLab docker registry"
    )
    parser.add_argument("--url", help="API address of GitLab", required=True, type=str)
    parser.add_argument("--token", help="Authentication token", required=True, type=str)
    parser.add_argument(
        "--project-id", help="Unique GitLab project ID", required=True, type=int
    )
    parser.add_argument(
        "--repository-id", help="Gitlab image repository id", required=True, type=int
    )
    parser.add_argument(
        "--tag",
        help="Tag name with value of TASK_NUMBER",
        required=True,
        type=str,
    )
    return parser.parse_args()


def main():
    cmd = args()
    gl = authenticate(cmd.url, cmd.token)
    pr = project(gl, cmd.project_id)
    rp = container_registry_repository(pr, cmd.repository_id)
    tags = rp.tags.list() if rp is not None else []
    for tag in tags:
        if tag.name == cmd.tag:
            tag.delete()


if __name__ == "__main__":
    main()
