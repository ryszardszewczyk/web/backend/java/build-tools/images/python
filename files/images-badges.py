#!/usr/bin/env python3
from argparse import ArgumentParser, Namespace

from gitlab.v4.objects import Project
from gitlab.base import RESTObject

from gitlab_function import authenticate
from gitlab_function import project
from base import logger

LOGGER = logger()


def args() -> Namespace:
    """
    Creates an ArgumentParser to run creating badges.
    :return: A Namespace instance.
    """
    parser = ArgumentParser(
        description="Bot automating different things related to GitLab"
    )
    parser.add_argument("--url", help="API address of GitLab", required=True, type=str)
    parser.add_argument("--token", help="Authentication token", required=True, type=str)
    parser.add_argument(
        "--project-id", help="Unique GitLab project ID", required=True, type=int
    )
    parser.add_argument(
        "--image",
        help="Name of the image published to Docker Hub",
        required=True,
        type=str,
    )
    parser.add_argument(
        "--version",
        help="Version of the image published to Docker Hub",
        required=True,
        type=str,
    )
    parser.add_argument("--base", help="Main branch name", required=True, type=str)
    return parser.parse_args()


def find_existing_badge(pr: Project, badge_name: str) -> RESTObject | None:
    """
    Checks if given badge exists.
    :param pr: Project instance
    :param badge_name: Badge name
    :return: If successful returns information about the badge, otherwise None.
    """
    badges = pr.badges.list()
    for badge in badges:
        if badge.name == badge_name:
            return badge
    return None


def update_badge(
    badge: RESTObject, badge_data: dict[str, str], image: str, version: str
) -> None:
    """
    Updates a given badge.
    :param badge: Information about the badge
    :param badge_data: Data about the badge to be updated
    :param image: Name of the docker image
    :param version: Version of the docker image
    :return: None.
    """
    LOGGER.info("Updating existing badge %s: %s", image, version)
    badge.image_url = badge_data["image_url"]
    badge.save()


def create_badge_data(image: str, version: str) -> dict[str, str]:
    """
    Creates a badge data about given image and its version.
    :param image: Name of the docker image
    :param version: Version of the docker image
    :return: Dictionary representing json data object.
    """
    LOGGER.info("Creating badge data for image %s: %s", image, version)
    badge_data = {
        "link_url": f"https://hub.docker.com/repository/docker/bananawhite98/{image}/general",
        "image_url": f"https://img.shields.io/badge/{image.replace('-', '_')}%20version-{version}-blue",
        "name": f"{image} version",
    }
    LOGGER.debug("Created badge data for image %s: %s", image, version)
    return badge_data


def create_badge(
    pr: Project, image: str, version: str, badge_data: dict[str, str]
) -> None:
    """
    Creates a badge about image and its version.
    :param pr: Project instance where badge is to be created
    :param image: Name of the docker image
    :param version: Version of the docker image
    :param badge_data: Badge data about the docker image
    :return: None.
    """
    LOGGER.info("Creating a new badge %s: %s", image, version)
    pr.badges.create(badge_data)


def main():
    cmd = args()
    gl = authenticate(cmd.url, cmd.token)
    pr = project(gl, cmd.project_id)
    badge_data = create_badge_data(cmd.image, cmd.version)
    badge = find_existing_badge(pr, f"{cmd.image} version")
    if badge:
        update_badge(badge, badge_data, cmd.image, cmd.version)
    else:
        create_badge(pr, cmd.image, cmd.version, badge_data)


if __name__ == "__main__":
    main()
